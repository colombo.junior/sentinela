#!/bin/bash

source ../venv/bin/activate

# Inicializa o Celery
celery -A sistema_gerenciamento worker -Q videos -l info --concurrency 3 &
