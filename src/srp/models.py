from django.db import models


class SRP(models.Model):
    FASE_ABERTURA = '1'
    FASE_DEFINICAO_INICIAL = '2'
    FASE_DEFINICAO_FINAL = '3'
    FASE_CHECKLIST = '4'
    FASE_ESTIMATIVA = '5'
    FASE_ANALISE_JURIDICA = '6'
    FASE_LICITANDO = '7'
    FASE_ASSINATURA = '8'
    FASE_VIGENTE = '9'
    FASE_ENCERRADA = '10'
    FASES = (
        (FASE_ABERTURA, '1: Abertura dos processos originais'),
        (FASE_DEFINICAO_INICIAL, '2: Definição de planilha de itens inicial'),
        (FASE_DEFINICAO_FINAL, '3: Definição de planilha de itens final, IRP e devolução'),
        (FASE_CHECKLIST, '4: Checklist documental e conferência do lançamento IRP'),
        (FASE_ESTIMATIVA, '5: Estimativa de quantitativos, análise, negociação e confirmação da IRP'),
        (FASE_ANALISE_JURIDICA, '6: Análise jurídica'),
        (FASE_LICITANDO, '7: Licitando'),
        (FASE_ASSINATURA, '8: Assinatura das atas'),
        (FASE_VIGENTE, '9: Atas vigentes'),
        (FASE_ENCERRADA, '10: Atas vencidas'),
    )

    NATUREZA_CONSUMO = 'C'
    NATUREZA_PERMANENTE = 'P'
    NATUREZA_CONSUMO_E_PERMANENTE = 'CP'
    NATUREZAS = (
        (NATUREZA_CONSUMO, 'Consumo'),
        (NATUREZA_PERMANENTE, 'Permanente'),
        (NATUREZA_CONSUMO_E_PERMANENTE, 'Consumo e permanente')
    )

    fase = models.CharField(choices=FASES, max_length=2)
    natureza_despesa = models.CharField(choices=NATUREZAS, max_length=2)
    numero_processo_original = models.CharField(max_length=50)
    nome_licitacao = models.CharField(max_length=200)
    ano_homologacao = models.DateField()
    validade_ata = models.DateField()
    numero_irp = models.CharField(max_length=10)
    numerp_srp = models.URLField()
    url_ata = models.URLField()


class ProdutoSRP(models.Model):
    srp = models.ForeignKey(SRP, on_delete=models.PROTECT)
    item = models.CharField(max_length=10)
    catmat = models.CharField(max_length=100)
    descricao_sumaria = models.CharField(max_length=1000)
    descricao_completa = models.CharField(max_length=10000)
    unidade_fornecimento = models.CharField(max_length=10)
    valor_unitario = models.FloatField()
    fornecedor = models.CharField(max_length=100)
    cnpj = models.CharField(max_length=20)
    fabricante = models.CharField(max_length=100)
    marca = models.CharField(max_length=100)
