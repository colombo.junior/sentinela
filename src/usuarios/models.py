from django.db import models
from django.contrib.auth.models import User


class Area(models.Model):
    nome = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return '%s' % self.nome

    class Meta:
        verbose_name_plural = "Áreas"
        ordering = ["nome", ]


# Opções padrão
tipo_telefone = [
    2 * ('Comercial',),
    2 * ('Celular',),
]

SEGUNDA, TERCA, QUARTA, QUINTA, SEXTA, SABADO = '1', '2', '3', '4', '5', '6'
DIAS_DA_SEMANA = (
    (SEGUNDA, 'Segunda'),
    (TERCA, 'Terça'),
    (QUARTA, 'Quarta'),
    (QUINTA, 'Quinta'),
    (SEXTA, 'Sexta'),
    (SABADO, 'Sábado'),
)
DIAS_DA_SEMANA_DICT = dict(DIAS_DA_SEMANA)

MANHA, TARDE, NOITE = '1', '2', '3'
TURNOS = (
    (MANHA, 'Manhã'),
    (TARDE, 'Tarde'),
    (NOITE, 'Noite'),
)
TURNOS_DICT = dict(TURNOS)


class Instituicao(models.Model):
    # Informações para o cadastro da instituição
    razao_social = models.CharField(max_length=2000, blank=False)
    nome_fantasia = models.CharField(max_length=2000, blank=True)
    cnpj = models.CharField(max_length=14, blank=True)
    cep = models.CharField(max_length=9, blank=False)
    estado = models.CharField(max_length=100, blank=False)
    municipio = models.CharField(max_length=500, blank=False)
    endereco = models.CharField(max_length=1000, blank=False)
    numero = models.CharField(max_length=6, blank=False)
    tipo_telefone = models.CharField(max_length=10, choices=tipo_telefone, blank=False)
    numero_telefone = models.CharField(max_length=15, blank=False)
    data_cadastro = models.DateField(auto_now_add=True)

    numero_slots_manha = models.PositiveIntegerField(default=0)
    numero_slots_tarde = models.PositiveIntegerField(default=0)
    numero_slots_noite = models.PositiveIntegerField(default=0)
    tem_aula_manha = models.BooleanField(default=False)
    tem_aula_tarde = models.BooleanField(default=False)
    tem_aula_noite = models.BooleanField(default=False)

    tem_aula_segunda = models.BooleanField(default=False)
    tem_aula_terca = models.BooleanField(default=False)
    tem_aula_quarta = models.BooleanField(default=False)
    tem_aula_quinta = models.BooleanField(default=False)
    tem_aula_sexta = models.BooleanField(default=False)
    tem_aula_sabado = models.BooleanField(default=False)

    # Cobrança
    ativo = models.BooleanField(default=False)
    saldo = models.FloatField(default=0)

    # Informações sobre as restirções legais dos docentes
    numero_aulas_maximo_diario_docentes = models.PositiveIntegerField(default=9)
    numero_aulas_maximo_docentes = models.PositiveIntegerField(default=19)
    numero_aulas_maximo_coordenadores = models.PositiveIntegerField(default=10)
    tempo_descanso_interjornada = models.TimeField(default='11:00')

    # Pode editar informações no site?
    pode_editar = models.BooleanField(default=True)

    def __str__(self):
        return '%s (%s)' % (self.nome_fantasia, self.municipio)

    class Meta:
        ordering = ('nome_fantasia', )
        verbose_name_plural = "Instituições"
        verbose_name = 'Instituição'

    def dias_com_aula(self):
        dias = (
            DIAS_DA_SEMANA[0][0] if self.tem_aula_segunda else None,
            DIAS_DA_SEMANA[1][0] if self.tem_aula_terca else None,
            DIAS_DA_SEMANA[2][0] if self.tem_aula_quarta else None,
            DIAS_DA_SEMANA[3][0] if self.tem_aula_quinta else None,
            DIAS_DA_SEMANA[4][0] if self.tem_aula_sexta else None,
            DIAS_DA_SEMANA[5][0] if self.tem_aula_sabado else None
        )

        # Remove os elementos None
        dias = list(filter((None).__ne__, dias))

        return dias

    def numero_de_dias_que_tem_aula(self):
        return len(self.dias_com_aula())

    def turnos_com_aula(self):
        turnos_com_aula = (
            (TURNOS[0][0], self.numero_slots_manha) if self.tem_aula_manha else None,
            (TURNOS[1][0], self.numero_slots_tarde) if self.tem_aula_tarde else None,
            (TURNOS[2][0], self.numero_slots_noite) if self.tem_aula_noite else None
        )

        # Remove os elementos None
        turnos_com_aula = list(filter((None).__ne__, turnos_com_aula))

        return turnos_com_aula


class Usuario(models.Model):
    # instituicao = models.ForeignKey(Instituicao, on_delete=models.SET_NULL, null=True, blank=True)
    usuario_django = models.OneToOneField(User, on_delete=models.PROTECT)
    prontuario = models.CharField(max_length=20, blank=True)

    data_cadastro = models.DateField(auto_now_add=True)
    data_ultimo_acesso = models.DateTimeField(null=True)

    TIPO_DOCENTE = 'D'
    TIPO_TECNICO_LABORATORIO = 'TL'
    TIPO_TECNICO_ADMINISTRATIVO = 'TA'
    TIPO_FUNCIONARIO_TERCEIRIZADO = 'FT'
    TIPO_ESTUDANTE = 'E'
    TIPO_ALUNO = TIPO_ESTUDANTE
    TIPOS = (
        (TIPO_DOCENTE, 'Docente'),
        (TIPO_TECNICO_LABORATORIO, 'Técnico Laboratório'),
        (TIPO_TECNICO_ADMINISTRATIVO, 'Técnico Administrativo'),
        (TIPO_FUNCIONARIO_TERCEIRIZADO, 'Funcionário Terceirizado'),
        (TIPO_ESTUDANTE, 'Estudante'),
    )
    tipo = models.CharField(choices=TIPOS, max_length=2, blank=False)

    def __str__(self):
        return '%s' % self.usuario_django.email
