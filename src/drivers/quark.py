import time
from .quark_python import QuarkCpp
from .quark_python import CH1, CH2, CH3, CH4
from .quark_python import HIGH, LOW
from .quark_python import PULL_NONE, PULL_DOWN, PULL_UP
from .quark_python import OPEN_DRAIN, PUSH_PULL
from .quark_python import ENCODER_CW_IS_POSITIVE, ENCODER_CCW_IS_POSITIVE
from .quark_python import ENCODER_QUADRATURE_X1, ENCODER_QUADRATURE_X2, ENCODER_QUADRATURE_X4


def time_ms():
    return 1e-6 * time.time_ns()


def time_us():
    return 1e-3 * time.time_ns()


class Quark:
    def __init__(self, serial_port_path, timeout_ms, max_retries):
        self.board = QuarkCpp(serial_port_path, timeout_ms, max_retries)

    def read_all(self):
        """
        It reads all the memory of the board. If high sample rates are desired,
        then it is recommended to call this method in the beginning of the loop.
        So it will acquire all the board data at once. Then use the get or read
        functions with the parameter from_buffer=True.

        :return: nothing.
        """
        self.board.read_all_cpp()

    def set_gpio_input_pull(self, channel: int, pull: int):
        """
        This method sets the input pull resistors of an input digital pin.

        :param channel: is the INPUT GPIO channel. Example: CH1.
        :param pull: is the pull resistor. It can be PULL_NONE, PULL_UP or PULL_DOWN.
        :return: nothing.
        """
        self.board.set_gpio_input_pull_cpp(channel, pull)

    def set_gpio_output_pull(self, channel: int, pull: int):
        """
        This method sets the input pull resistors of an input digital pin.

        :param channel: is the INPUT GPIO channel. Example: CH1.
        :param pull: is the pull resistor. It can be PULL_NONE, PULL_UP or PULL_DOWN.
        :return: nothing.
        """
        self.board.set_gpio_output_pull_cpp(channel, pull)

    def set_gpio_output_mode(self, channel: int, mode: int):
        """
        This method sets the driver mode of an output digital pin.

        :param channel: is the output gpio channel. Example: CH1.
        :param mode: is the driver mode. It can be OPEN_DRAIN or PUSH_PULL.
        :return: nothing.
        """
        self.board.set_gpio_output_mode_cpp(channel, mode)

    def digital_read(self, channel: int, from_buffer=False):
        """
        This method reads the logic state of a digital input pin.

        :param channel: channel is the input gpio channel. Example: CH1.
        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: HIGH or LOW.
        """
        return self.board.digital_read_cpp(channel, from_buffer)

    def digital_write(self, channel: int, state: int):
        """
        This method writes HIGH or LOW value to a output digital pin.

        :param channel: is the output gpio channel. Example: CH3.
        :param state: state HIGH or LOW.
        :return: nothing.
        """
        self.board.digital_write_cpp(channel, state)

    def analog_read(self, channel: int, from_buffer=False):
        """
        This method reads the analog value on an analog channel.

        :param channel: is the analog channel. Example: CH3.
        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the voltage (in V) on the analog pin.
        """
        return self.board.analog_read_cpp(channel, from_buffer)

    def adc_enable(self):
        """
        This method enables the ADC.

        :return: nothing.
        """
        self.board.adc_enable_cpp()

    def adc_disable(self):
        """
        This method disables the ADC.

        :return: nothing.
        """
        self.board.adc_disable_cpp()

    def pwm_write(self, channel: int, duty: float):
        """
        This method sets the duty cycle of some PWM channel.

        :param channel: the PWM channel. Example: CH3.
        :param duty: a real number between 0 and 1.
        :return: nothing.
        """
        self.board.pwm_write_cpp(channel, duty)

    def pwm_set_frequency(self, freq_in_hz: float):
        """
        This method sets the PWM frequency.

        :param freq_in_hz: the frequency (in Hz) of the PWM signal.
        :return: nothing.
        """
        self.board.pwm_set_frequency_cpp(freq_in_hz)

    def pwm_get_frequency(self, from_buffer=False):
        """
        This method returns the actual PWM frequency.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: a real number representing the PWM frequency.
        """
        return self.board.pwm_get_frequency_cpp(from_buffer)

    def ds18_get_temperature(self, channel, from_buffer=False):
        """
        This method will read the temperature measured by DS18 devices. It is
        considered that only one sensor (of any kind) is attached to each
        OneWire channel. Warning: this bus is very slow. This command may take
        up 100 ms.

        :param channel: it is the channel that the sensor is attached to.
        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: The temperature in ºC. A very small temperature will be reported
                 is the sensor is disconnected.
        """
        return self.board.ds18_get_temperature_cpp(channel, from_buffer)

    def ds18_enable(self, channel):
        """
        This method will try to enable a DS18 device on a given channel.
        Warning: this bus is very slow. This command may take up 100 ms.

        :param channel: it is the channel that the sensor is attached to.
        :return:
        """
        self.board.ds18_enable_cpp(channel)

    def ds18_disable(self, channel):
        """
        This method will disable a DS18 device on a given channel.

        :param channel: it is the channel that the sensor is attached to.
        :return:
        """
        self.board.ds18_disable_cpp(channel)

    def ds18_is_enabled(self, channel):
        """
        This method will verify if the sensor attached to a given channel
        is enabled.

        :param channel: it is the channel that the sensor is attached to.
        :return: True if the sensor is enabled and False otherwise.
        """
        return self.board.ds18_is_enabled_cpp(channel)

    def encoder_1_read_pulse_count(self, from_buffer=False):
        """
        This method reads the encoder 1 pulse count value.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder pulse count value.
        """
        return self.board.encoder_1_read_pulse_count_cpp(from_buffer)

    def encoder_2_read_pulse_count(self, from_buffer=False):
        """
        This method reads the encoder 2 pulse count value.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder pulse count value.
        """
        return self.board.encoder_2_read_pulse_count_cpp(from_buffer)

    def encoder_1_read_error_count(self, from_buffer=False):
        """
        This method reads the encoder 1 error count value.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder error count value.
        """
        return self.board.encoder_1_read_pulse_count_cpp(from_buffer)

    def encoder_2_read_error_count(self, from_buffer=False):
        """
        This method reads the encoder 2 error count value.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder error count value.
        """
        return self.board.encoder_2_read_pulse_count_cpp(from_buffer)

    def encoder_1_read_time(self, from_buffer=False):
        """
        This method reads the encoder 1 time measurement between two rising
        edges on CHA

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder count value.
        """
        return self.board.encoder_1_read_time_cpp(from_buffer)

    def encoder_2_read_time(self, from_buffer=False):
        """
        This method reads the encoder 1 time measurement between two rising
        edges on CHA

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: the encoder count value.
        """
        return self.board.encoder_2_read_time_cpp(from_buffer)

    def encoder_read_all_measurements(self):
        self.board.encoder_read_all_measurements()

    def encoder_1_set_direction(self, positive_direction):
        """
        This method sets the encoder 1 positive direction. It has the
        equivalent effect of swapping the encoder cables (CHA and CHB).

        :param positive_direction: direction it can be ENCODER_CW_IS_POSITIVE
               or ENCODER_CCW_IS_POSITIVE.
        :return: nothing.
        """
        self.board.encoder_1_set_direction_cpp(positive_direction)

    def encoder_2_set_direction(self, positive_direction):
        """
        This method sets the encoder 2 positive direction. It has the
        equivalent effect of swapping the encoder cables (CHA and CHB).

        :param positive_direction: direction it can be ENCODER_CW_IS_POSITIVE
               or ENCODER_CCW_IS_POSITIVE.
        :return: nothing.
        """
        self.board.encoder_2_set_direction_cpp(positive_direction)

    def encoder_1_reset_counting(self):
        """
        This method resets the encoder 1 count value.

        :return: nothing.
        """
        self.board.encoder_1_reset_counting_cpp()

    def encoder_2_reset_counting(self):
        """
        This method resets the encoder 2 count value.

        :return: nothing.
        """
        self.board.encoder_2_reset_counting_cpp()

    def encoder_1_enable(self):
        """
        This method enables the encoder 1.

        :return: nothing.
        """
        self.board.encoder_1_enable_cpp()

    def encoder_2_enable(self):
        """
        This method enables the encoder 2.

        :return: nothing.
        """
        self.board.encoder_2_enable_cpp()

    def encoder_1_disable(self):
        """
        This method disables the encoder 1.

        :return: nothing.
        """
        self.board.encoder_1_disable_cpp()

    def encoder_2_disable(self):
        """
        This method disables the encoder 2.

        :return: nothing.
        """
        self.board.encoder_2_disable_cpp()

    def encoder_1_set_quadrature(self, quadrature):
        """
        This method sets the encoder 1 positive direction. It has the
        equivalent effect of swapping the encoder cables (CHA and CHB).

        :param quadrature: it can be ENCODER_QUADRATURE_1X,
               ENCODER_QUADRATURE_2X or ENCODER_QUADRATURE_4X.
        :return: nothing.
        """
        self.board.encoder_1_set_quadrature_cpp(quadrature)

    def encoder_2_set_quadrature(self, quadrature):
        """
        This method sets the encoder 2 positive direction. It has the
        equivalent effect of swapping the encoder cables (CHA and CHB).

        :param quadrature: it can be ENCODER_QUADRATURE_1X,
               ENCODER_QUADRATURE_2X or ENCODER_QUADRATURE_4X.
        :return: nothing.
        """
        self.board.encoder_2_set_quadrature_cpp(quadrature)

    def encoder_1_set_timeout(self, msecs):
        """
        This method sets the timeout value of encoder 1. If no new pulse is received
        within this time, the encoder will be considered stopped.

        :param msecs: is a positive integer representing the timeout in milliseconds.
               The smallest value is 0 and the maximum is 255.
        :return: nothing.
        """
        self.board.encoder_1_set_timeout_cpp(msecs)

    def encoder_2_set_timeout(self, msecs):
        """
        This method sets the timeout value of encoder 2. If no new pulse is received
        within this time, the encoder will be considered stopped.

        :param msecs: is a positive integer representing the timeout in milliseconds.
               The smallest value is 0 and the maximum is 255.
        :return: nothing.
        """
        self.board.encoder_2_set_timeout_cpp(msecs)

    def encoder_1_set_filter_size(self, size):
        """
        The encoder driver in the board has a moving average filter. The window size
        can be changed using this method. It is only used to filter the time
        measurements.

        :param size: is a positive integer with maximum as 16 and minimum as 1.
        :return: nothing.
        """
        self.board.encoder_1_set_filter_size_cpp(size)

    def encoder_2_set_filter_size(self, size):
        """
        The encoder driver in the board has a moving average filter. The window size
        can be changed using this method. It is only used to filter the time
        measurements.

        :param size: is a positive integer with maximum as 16 and minimum as 1.
        :return: nothing.
        """
        self.board.encoder_2_set_filter_size_cpp(size)

    def encoder_1_set_rel_max_rate(self, max_rate):
        """
        The encoder time measurement driver in the board has a relative rate limitation.
        It is implemented the following algorithm:

                (new_raw_measurement - old_measurement)
        rate = -----------------------------------------
                         old_measurement

        If abs(rate) is bigger than max_rate, then the new measurement will be calculated as:

        new_measurement = (1 + sign(rate) * (max_rate / 100)) * old_measurement

        This prevents false readings due noisy signals or unbalanced encoders.

        :param max_rate: a positive integer number between 0 and 200 (in %).
        :return: nothing.
        """
        self.board.encoder_1_set_rel_max_rate_cpp(max_rate)

    def encoder_2_set_rel_max_rate(self, max_rate):
        """
        The encoder time measurement driver in the board has a relative rate limitation.
        It is implemented the following algorithm:

                (new_raw_measurement - old_measurement)
        rate = -----------------------------------------
                         old_measurement

        If abs(rate) is bigger than max_rate, then the new measurement will be calculated as:

        new_measurement = (1 + sign(rate) * (max_rate / 100)) * old_measurement

        This prevents false readings due noisy signals or unbalanced encoders.

        :param max_rate: a positive integer number between 0 and 200 (in %).
        :return: nothing.
        """
        self.board.encoder_2_set_rel_max_rate_cpp(max_rate)

    def get_firmware_major_version(self, from_buffer=False):
        """
        This method reads the firmware major version.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: a integer representing the major version.
        """
        return self.board.get_firmware_major_version_cpp(from_buffer)

    def get_firmware_minor_version(self, from_buffer=False):
        """
        This method reads the firmware minor version.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: a integer representing the minor version.
        """
        return self.board.get_firmware_minor_version_cpp(from_buffer)

    def get_firmware_patch_version(self, from_buffer=False):
        """
        This method reads the firmware patch version.

        :param from_buffer: if true it will communicate with the board. If
               false it reads from the buffer.
        :return: a integer representing the patch version.
        """
        return self.board.get_firmware_patch_version_cpp(from_buffer)

    def get_who_am_i(self):
        """
        This method reads the WHO_AM_I value.

        :return: it is expected to read the value 29.
        """
        return self.board.get_who_am_i_cpp()

    def close(self):
        """
        This method closes the serial port.

        :return: nothing
        """
        self.board.close_cpp()


if __name__ == '__main__':
    board = Quark('/dev/ttyACM0')

    try:
        print("Press Ctrl-C to terminate the test")

        while True:
            board.digital_write(CH3, LOW)

            time.sleep(100e-3)
            board.digital_write(CH3, HIGH)
            time.sleep(200e-3)
            board.digital_write(CH3, LOW)
            time.sleep(100e-3)
            board.digital_write(CH3, HIGH)
            time.sleep(600e-3)

    except KeyboardInterrupt:
        pass

    board.close()
