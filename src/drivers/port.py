from .quark import *
from scipy.io import savemat
import time


def executar_codigo(codigo, porta_serial, nome_arquivo_matlab=None):
    dados = dict()

    # Abre a comunicação com a placa
    timeout_ms = 1
    max_retries = 3
    placa = Quark('%s' % porta_serial, timeout_ms, max_retries)

    # Roda o ensaio
    try:
        exec(codigo)
        erro = False
    except:
        erro = True

    # Fecha a porta serial
    placa.close()

    if nome_arquivo_matlab is not None:
        savemat('%s' % nome_arquivo_matlab, dados)

    # Espera 10 segundos entre um experimento e outro
    time.sleep(5)

    return erro



