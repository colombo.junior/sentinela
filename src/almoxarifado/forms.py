from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User

from .models import *


class FormNovoFabricante(forms.ModelForm):
    class Meta:
        model = Fabricante
        fields = ('nome', 'site', )


class FormEditarFabricante(forms.ModelForm):
    class Meta:
        model = Fabricante
        fields = ('nome', 'site', )


class FormNovoModelo(forms.ModelForm):
    class Meta:
        model = Modelo
        fields = ('categoria', 'fabricante', 'nome', 'site', 'foto', )


class FormEditarModelo(forms.ModelForm):
    class Meta:
        model = Modelo
        fields = ('categoria', 'fabricante', 'nome', 'site', 'foto', )


class FormCriarTipoMaterial(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ('nome', )
