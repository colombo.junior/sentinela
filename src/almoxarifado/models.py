from django.db import models
from django.utils.deconstruct import deconstructible
from django.conf.global_settings import MEDIA_ROOT
from ckeditor_uploader.fields import RichTextUploadingField

from usuarios.models import Usuario, Area

import os
from uuid import uuid4


@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)


class Sala(models.Model):
    nome = models.CharField(max_length=200)
    sigla = models.CharField(max_length=20)
    area = models.ForeignKey(
        Area,
        on_delete=models.PROTECT,
        related_name='sala_area'
    )
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.sigla

    class Meta:
        verbose_name_plural = "Salas"
        ordering = ["sigla", ]


class Categoria(models.Model):
    nome = models.CharField(max_length=200)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.nome

    class Meta:
        verbose_name_plural = "Categorias"
        ordering = ["nome", ]


class Fabricante(models.Model):
    nome = models.CharField(max_length=200)
    site = models.URLField()
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.nome

    class Meta:
        verbose_name_plural = 'Fabricantes'
        ordering = ['data_criacao', ]


class Modelo(models.Model):
    fabricante = models.ForeignKey(
        Fabricante,
        on_delete=models.PROTECT,
        related_name='modelo_fabricante',
        blank=True,
        null=True
    )
    nome = models.CharField(max_length=200)
    foto = models.ImageField(upload_to=UploadToPathAndRename(os.path.join(MEDIA_ROOT, 'upload', 'modelo')))
    site = models.URLField()
    data_criacao = models.DateTimeField(auto_now_add=True)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)

    def __str__(self):
        return '%s' % self.nome

    class Meta:
        verbose_name_plural = "Modelos"
        ordering = ["nome", ]


class Item(models.Model):
    TIPO_EQUIPAMENTO = 'E'
    TIPO_MATERIAL_CONSUMO = 'MC'
    TIPOS_ITENS = (
        (TIPO_EQUIPAMENTO, 'Equipamento'),
        (TIPO_MATERIAL_CONSUMO, 'Material de consumo'),
    )
    tipo = models.CharField(
        max_length=2,
        choices=TIPOS_ITENS,
        default=TIPO_MATERIAL_CONSUMO
    )

    # nome = models.CharField(max_length=1000)
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    modelo = models.ForeignKey(Modelo, on_delete=models.PROTECT)
    patrimonio = models.CharField(max_length=100, default='', blank=True)
    foto = models.ImageField(upload_to=UploadToPathAndRename(os.path.join(MEDIA_ROOT, 'upload', 'almoxarifado_item')))

    UNIDADE_KG = 'KG'
    UNIDADE_ROLO = 'R'
    UNIDADE_METRO = 'M'
    UNIDADE_UNIDADE = 'U'
    UNIDADE_ = ''
    OPCOES_UNIDADES = (
        (UNIDADE_KG, 'KG'),
        (UNIDADE_ROLO, 'Rolo'),
        (UNIDADE_METRO, 'Metro'),
        (UNIDADE_UNIDADE, 'Unidade'),
    )
    unidade_medida = models.CharField(
        max_length=2,
        choices=TIPOS_ITENS,
        default=TIPO_MATERIAL_CONSUMO
    )
    quantidade = models.DecimalField(max_digits=8, decimal_places=2, blank=True, default=0)

    # Sobre a aquisição do item
    preco_aquisicao = models.DecimalField(max_digits=8, decimal_places=2, blank=True)

    # Sobre a localização do item
    localizacao_padrao = models.ForeignKey(
        Sala,
        on_delete=models.PROTECT,
        related_name='equipamento_localizacaopadrao',
        blank=True
    )
    localizacao_atual = models.ForeignKey(
        Sala,
        on_delete=models.PROTECT,
        related_name='equipamento_localizacaoatual',
        blank=True
    )

    def __str__(self):
        return '%s: %s' % (self.categoria.nome, self.modelo.nome)

    class Meta:
        verbose_name_plural = "Equipamentos"
        ordering = ['categoria', 'modelo', ]


class OcorrenciaItem(models.Model):
    item = models.ForeignKey(
        Item,
        on_delete=models.PROTECT
    )
    texto = RichTextUploadingField()
    data_criacao = models.DateTimeField(auto_now_add=True)


class SolicitacaoEmprestimo(models.Model):
    # Sobre quem solicita o material
    requerente = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    justificativa = models.CharField(max_length=2000, blank=False)

    # Sobre a entrega do material
    horario_entrega = models.DateTimeField(auto_now_add=True)
    sala_entrega = models.ForeignKey(Sala, on_delete=models.PROTECT)

    # Quem vai separar o material?
    tecnico = models.ForeignKey(
        Usuario,
        on_delete=models.PROTECT,
        related_name='solicitacaoemprestimo_tecnico',
        blank=True,
        null=True
    )

    # Sobre o status do empréstimo
    STATUS_REGISTRADO = '1'
    STATUS_EM_SEPARACAO = '2'  # um ténico foi atribuído para separar o material solicitado
    STATUS_DISPONIVEL = '3'
    STATUS_RETIRADO = '4'
    STATUS_PENDENTE = '5'
    STATUS_FINALIZADO = '6'
    OPCOES_STATUS = (
        (STATUS_REGISTRADO, 'Registrado'),
        (STATUS_EM_SEPARACAO, 'Em separação'),
        (STATUS_DISPONIVEL, 'Disponível'),
        (STATUS_RETIRADO, 'Retirado'),
        (STATUS_PENDENTE, 'Pendente'),
        (STATUS_FINALIZADO, 'Finalizado')
    )
    status = models.CharField(
        choices=OPCOES_STATUS,
        max_length=2,
        blank=False,
        default=STATUS_REGISTRADO
    )

    data_registro = models.DateTimeField(auto_now_add=True)
    data_em_seperacao = models.DateTimeField(auto_now_add=True)
    data_disponivel = models.DateTimeField(auto_now_add=True)
    data_retirado = models.DateTimeField(auto_now_add=True)
    data_devolucao = models.DateTimeField(auto_now_add=True)


class ItemSolicitado(models.Model):
    # Sobre a solicitação
    solicitacao = models.ForeignKey(SolicitacaoEmprestimo, on_delete=models.PROTECT)
    item = models.ForeignKey(Item, on_delete=models.PROTECT)

    # O requerente preenche
    quantidade_solicitada = models.PositiveIntegerField(default=1)

    # O técnico preenche
    quantidade_aprovada = models.PositiveIntegerField(default=1)
    justificativa = models.CharField(max_length=1000, blank=True)

    # Avaliação
    pronto_no_horario_requerente = models.BooleanField(default=True)
    problema_reportado_requerente = models.BooleanField(default=False)
    descricao_problema_requerente = models.CharField(max_length=100)
    devolvido = models.BooleanField(default=False)                      # técnico
    item_voltou_com_defeito = models.BooleanField(default=False)        # técnico
    quantidade_retornada = models.IntegerField(default=0)               # técnico





