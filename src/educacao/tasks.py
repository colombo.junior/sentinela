from __future__ import absolute_import
from celery import shared_task

from django.conf import settings

import time
import datetime
import pytz
import os
import subprocess
import shlex

try:
    from src.lab_remoto.models import Ensaio, KitDidatico, ExecucaoEnsaio, ParametroInformado, ParametroParaSerInformado
    from src.lab_remoto.models import ParametroDoKit
    from src.educacao.models import ExperimentoLabRemoto
    from src.drivers.port import executar_codigo
except:
    from lab_remoto.models import Ensaio, KitDidatico, ExecucaoEnsaio, ParametroInformado, ParametroParaSerInformado
    from lab_remoto.models import ParametroDoKit
    from educacao.models import ExperimentoLabRemoto
    from drivers.port import executar_codigo


def executar_experimento(id_experimento, id_execucao):
    experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
    ensaio = experimento.ensaio
    kit = ensaio.kit
    codigo = ensaio.codigo_executavel
    execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
    usuario = execucao.usuario

    parametros_kit = ParametroDoKit.objects.filter(
        kit=ensaio.kit,
    )

    parametros_informados = ParametroInformado.objects.filter(
        ensaio=ensaio,
        usuario=usuario,
        execucao=execucao,
    )

    # Substituição dos parâmetros
    for parametro in parametros_informados:
        sigla = parametro.parametro.sigla
        valor_informado = parametro.valor_informado
        codigo = codigo.replace('${%s}' % sigla, str(valor_informado))

    for parametro in parametros_kit:
        sigla = parametro.sigla
        valor = parametro.valor
        codigo = codigo.replace('${%s}' % sigla, str(valor))

    # Registra o horário de início do experimento
    execucao.data_inicio = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    execucao.save()

    # O kit agora está ocupado
    kit.ocupado = True
    kit.save()

    # Finaliza a montagem do código com o nome do arquivo matlab
    now = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    nome_arquivo = '%s_%s.mat' % (usuario.prontuario, now)
    nome_arquivo_completo = os.path.join(settings.MEDIA_ROOT, 'upload', 'dados_ensaios', nome_arquivo)

    # Salva o código e muda o estado para executando
    execucao.codigo = codigo
    execucao.estado = execucao.ESTADO_EXECUTANDO
    execucao.save()

    if (len(kit.webcam) > 0) and (execucao.filmar is True):
        webcam = kit.webcam
        cmd_gravar_video = kit.comando_gravar_video
        nome_arquivo_video = '%s_%s' % (usuario.prontuario, now)
        duracao_experimento = parametros_informados.get(parametro__sigla='tend').valor_informado

        # Essas linhas são para o computador de desenvolvimento
        # cmd = "ffmpeg -f v4l2 -framerate 30 -video_size 640x480 -i ${WEBCAM} -t ${DURACAO_EXPERIMENTO} ${NOME_ARQUIVO_VIDEO}.h264"
        # nome_arquivo_video = '%s_%s.mkv' % (usuario.prontuario, now)
        # duracao_experimento = 5

        # Nome completo do arquivo de vídeo
        nome_arquivo_video_completo = os.path.join(settings.MEDIA_ROOT, 'upload', 'dados_ensaios', nome_arquivo_video)

        cmd_gravar_video = cmd_gravar_video.replace('${WEBCAM}', webcam)
        cmd_gravar_video = cmd_gravar_video.replace('${NOME_ARQUIVO_VIDEO}', nome_arquivo_video_completo)
        cmd_gravar_video = cmd_gravar_video.replace('${DURACAO_EXPERIMENTO}', str(duracao_experimento + 4))

        extensao = ''
        if '.mkv' in cmd_gravar_video:
            extensao = '.mkv'
        elif '.mp4' in cmd_gravar_video:
            extensao = '.mp4'
        elif '.h264' in cmd_gravar_video:
            extensao = '.h264'
        elif '.avi' in cmd_gravar_video:
            extensao = '.avi'
        elif '.gif' in cmd_gravar_video:
            extensao = '.gif'

        # Registra o arquivo de vídeo
        execucao.arquivo_video = '/'.join(['upload', 'dados_ensaios', nome_arquivo_video + extensao])

        # Executa o comando para gravar vídeo
        processo_gravacao = subprocess.Popen(shlex.split(cmd_gravar_video))

        # Espera o ffmpeg começar a gravar
        time.sleep(5)

    # Executa o experimento
    erro = executar_codigo(codigo, kit.porta_serial, nome_arquivo_completo)

    if (len(kit.webcam) > 0) and (execucao.filmar is True):
        # Registra que está renderizando o vídeo
        execucao.estado = execucao.ESTADO_RENDERIZANDO_VIDEO

        # Reduz o tamanho do vídeo
        caminho_absoluto_video_com_extensao = nome_arquivo_video_completo + extensao
        # diminuir_video(id_execucao, caminho_absoluto_video_com_extensao)
        diminuir_video_assincrono.delay(id_execucao, caminho_absoluto_video_com_extensao)
    else:
        execucao.estado = execucao.ESTADO_FINALIZADO

    # Registra se o experimento foi bem sucedido
    if erro is True:
        execucao.resultado = execucao.RESULTADO_FALHA
    else:
        execucao.resultado = execucao.RESULTADO_SUCESSO

    # Registra o horário de término do experimento e o arquivo matlab
    execucao.data_termino = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    execucao.arquivo = '/'.join(['upload', 'dados_ensaios', nome_arquivo])
    execucao.save()

    # O kit agora está livre
    ensaio.kit.ocupado = False
    ensaio.kit.save()


@shared_task
def executar_experimento_assincrono(id_experimento, id_execucao):
    executar_experimento(id_experimento, id_execucao)


def diminuir_video(id_execucao, caminho_absoluto_video_com_extensao):
    nome_arquivo_com_extensao = caminho_absoluto_video_com_extensao.split('/')[-1]

    # Reduz o tamanho do vídeo
    cmd_reduz_video = "ffmpeg -i ${input_file} -threads 1 -vcodec libx265 -x265-params crf=28:pools=none ${output_file}"
    cmd_reduz_video = cmd_reduz_video.replace('${input_file}', caminho_absoluto_video_com_extensao)
    cmd_reduz_video = cmd_reduz_video.replace('${output_file}', '/tmp/' + nome_arquivo_com_extensao)
    os.system(cmd_reduz_video)

    # Remove o vídeo original e move o arquivo (menor) em lugar do arquivo original
    cmd_rm_video = 'rm %s' % (caminho_absoluto_video_com_extensao)
    os.system(cmd_rm_video)
    cmd_mv_video = 'mv %s %s' % ('/tmp/' + nome_arquivo_com_extensao, caminho_absoluto_video_com_extensao)
    os.system(cmd_mv_video)

    # Registra o término do processo
    execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
    execucao.estado = execucao.ESTADO_FINALIZADO
    execucao.save()


@shared_task
def diminuir_video_assincrono(id_execucao, caminho_absoluto_video_com_extensao):
    diminuir_video(id_execucao, caminho_absoluto_video_com_extensao)
