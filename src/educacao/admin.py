from django.contrib import admin
from .models import *


class AdminTema(admin.ModelAdmin):
    list_display = ('nome', )


class AdminDisciplina(admin.ModelAdmin):
    list_display = ('nome', 'sigla', 'turma', )


class AdminAvaliacao(admin.ModelAdmin):
    list_display = ('disciplina', 'tipo', 'numero', )


class AdminQuestao(admin.ModelAdmin):
    list_display = ('tipo', )


class AdminQuestaoAvaliacao(admin.ModelAdmin):
    list_display = ('avaliacao', 'questao', 'numero', )


class AdminAlternativa(admin.ModelAdmin):
    list_display = ('questao', 'codigo', 'resposta_correta', )


class AdminResposta(admin.ModelAdmin):
    list_display = ('questao', 'questao_avaliacao', 'aluno', )


class AdminAnexoResposta(admin.ModelAdmin):
    list_display = ('resposta', 'data_criacao', )


admin.site.register(Disciplina, AdminDisciplina)
admin.site.register(Avaliacao, AdminAvaliacao)
admin.site.register(QuestaoAvaliacao, AdminQuestaoAvaliacao)
admin.site.register(Questao, AdminQuestao)
admin.site.register(Alternativa, AdminAlternativa)
admin.site.register(Resposta, AdminResposta)
admin.site.register(AnexoResposta, AdminAnexoResposta)

