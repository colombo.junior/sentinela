# Generated by Django 2.2.9 on 2020-07-11 23:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lab_remoto', '0001_initial'),
        ('educacao', '0003_anexorelatorio_experimentolabremoto'),
    ]

    operations = [
        migrations.AddField(
            model_name='experimentolabremoto',
            name='ensaio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='lab_remoto.Ensaio'),
        ),
        migrations.AddField(
            model_name='anexorelatorio',
            name='relatorio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Avaliacao'),
        ),
    ]
