from django.db import models
from django.utils.deconstruct import deconstructible
from django.conf.global_settings import MEDIA_ROOT
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.dispatch import receiver

from lab_remoto.models import KitDidatico, Ensaio
from usuarios.models import Usuario

import os
from uuid import uuid4
import datetime


@deconstructible
class Caminho(object):
    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)


class Disciplina(models.Model):
    nome = models.CharField(max_length=100)
    sigla = models.CharField(max_length=10)
    numero_aulas = models.PositiveSmallIntegerField(default=19)
    data_criacao = models.DateTimeField(auto_now_add=True)
    professores = models.ManyToManyField(Usuario, blank=True)
    turma = models.CharField(max_length=10)
    objetivo = RichTextUploadingField()
    icone = models.FileField(
        upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'disciplinas'))
    )
    visivel = models.BooleanField(default=True)
    criterios_avaliacao = RichTextUploadingField()
    bibliografia = RichTextUploadingField()

    # Para ingressar
    senha = models.CharField(max_length=20, blank=True)
    aberto_para_inscricoes = models.BooleanField(default=False)
    inscritos = models.ManyToManyField(
        Usuario,
        related_name='disciplina_inscritos',
        blank=True
    )

    numero_provas = models.PositiveSmallIntegerField(default=0)
    numero_relatorios = models.PositiveSmallIntegerField(default=0)
    numero_trabalhos = models.PositiveSmallIntegerField(default=0)
    numero_listas = models.PositiveSmallIntegerField(default=0)
    numero_matriculas = models.PositiveSmallIntegerField(default=0)
    numero_seminarios = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return '%s (%s)' % (self.sigla, self.turma)

    class Meta:
        verbose_name_plural = "Disciplinas"
        ordering = ["-turma", ]


class Aula(models.Model):
    disciplina = models.ForeignKey(Disciplina, on_delete=models.PROTECT)
    descricao = RichTextUploadingField()
    data_criacao = models.DateTimeField(auto_now_add=True)
    data_da_aula = models.DateTimeField()
    numero = models.PositiveSmallIntegerField(default=1)
    visivel = models.BooleanField(default=False)

    def __str__(self):
        return 'Aula %s (%s)' % (self.numero, self.disciplina.sigla)

    class Meta:
        verbose_name_plural = "Aulas"
        ordering = ["numero", ]


class ArtigoAula(models.Model):
    aula = models.ForeignKey(Aula, on_delete=models.PROTECT)
    data_criacao = models.DateField(auto_now_add=True)
    titulo = models.CharField(max_length=1000)
    conteudo = RichTextUploadingField()


class AnexoAula(models.Model):
    TIPO_CODIGO_FONTE = 'CF'
    TIPO_DOCUMENTO = 'D'
    TIPO_IMAGEM = 'I'
    TIPO_OUTROS = 'O'
    TIPOS = (
        (TIPO_CODIGO_FONTE, 'Código fonte'),
        (TIPO_DOCUMENTO, 'Documento'),
        (TIPO_IMAGEM, 'Imagem'),
        (TIPO_OUTROS, 'Outros'),
    )

    aula = models.ForeignKey(Aula, on_delete=models.PROTECT)
    nome_arquivo = models.CharField(max_length=100)
    tipo = models.CharField(max_length=2, choices=TIPOS, default=TIPO_DOCUMENTO)
    arquivo = models.FileField(upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'anexo_aula')))
    descricao = RichTextUploadingField()
    data_criacao = models.DateField(auto_now_add=True)


class Matricula(models.Model):
    disciplina = models.ForeignKey(Disciplina, on_delete=models.PROTECT)
    aluno = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)


##############
# Avaliações #
##############
class Questao(models.Model):
    TIPO_MULTIPLA_ESCOLHA = 'ME'
    TIPO_VERDADEIRO_FALSO = 'VF'
    TIPO_DISSERATIVA = 'D'
    TIPOS_QUESTOES = [
        (TIPO_MULTIPLA_ESCOLHA, 'Múltipla escolha'),
        (TIPO_VERDADEIRO_FALSO, 'Verdadeiro ou falso'),
        (TIPO_DISSERATIVA, 'Dissertativa'),
    ]

    tipo = models.CharField(max_length=2, choices=TIPOS_QUESTOES)
    enunciado = RichTextUploadingField(blank=True)
    data_criacao = models.DateTimeField(auto_now_add=True)
    numero_respostas = models.PositiveIntegerField(default=0)
    nota_media = models.FloatField(default=0)
    desvio_padrao_nota = models.FloatField(default=0)
    numero_alternativas = models.PositiveIntegerField(default=0)
    nota_maxima = models.FloatField(default=1)

    class Meta:
        verbose_name_plural = "Questões"


class Alternativa(models.Model):
    OPCAO_VERDADEIRO = 'V'
    OPCAO_FALSO = 'F'
    OPCOES = (
        (OPCAO_VERDADEIRO, 'Verdadeiro'),
        (OPCAO_FALSO, 'Falso'),
    )

    questao = models.ForeignKey(Questao, on_delete=models.PROTECT)
    codigo = models.CharField(max_length=10, default='a')
    enunciado = RichTextUploadingField()
    data_criacao = models.DateTimeField(auto_now_add=True)
    resposta_correta = models.CharField(
        blank=True,
        max_length=2,
        choices=OPCOES
    )
    feedback = RichTextUploadingField(blank=True)
    nota_maxima = models.FloatField(default=1)


class Avaliacao(models.Model):
    AVALIACAO_PROVA = 'P'
    AVALIACAO_TRABALHO = 'T'
    AVALIACAO_RELATORIO = 'R'
    AVALIACAO_SEMINARIO = 'S'
    AVALIACAO_LISTA_EXERCICIOS = 'L'
    AVALIACAO_TIPOS = (
        (AVALIACAO_PROVA, 'Prova'),
        (AVALIACAO_TRABALHO, 'Trabalho'),
        (AVALIACAO_RELATORIO, 'Relatório'),
        (AVALIACAO_SEMINARIO, 'Seminário'),
        (AVALIACAO_LISTA_EXERCICIOS, 'Lista de exercícios'),
    )

    VISIBILIDADE_VISIVEL = 'V'
    VISIBILIDADE_OCULTO = 'O'
    VISIBILIDADE_VISIVEL_APOS_A_DATA = 'A'
    VISIBILIDADE_OPCOES = (
        (VISIBILIDADE_OCULTO, 'Oculto'),
        (VISIBILIDADE_VISIVEL, 'Visível'),
        (VISIBILIDADE_VISIVEL_APOS_A_DATA, 'Visível após a data'),
    )

    # Sobre a avaliação
    tipo = models.CharField(
        max_length=2,
        choices=AVALIACAO_TIPOS
    )
    titulo = models.CharField(max_length=100)
    descricao = RichTextUploadingField(blank=True)
    individual = models.BooleanField(default=True)
    numero_maximo_integrantes_grupo = models.IntegerField(default=3)
    numero = models.PositiveSmallIntegerField(default=1)
    numero_questoes = models.SmallIntegerField(default=1)
    nota_maxima = models.FloatField(default=10)

    # Identificação da avaliação
    disciplina = models.ForeignKey(Disciplina, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)

    # Sobre a visibilidade
    visibilidade = models.CharField(
        max_length=2,
        choices=VISIBILIDADE_OPCOES,
        default=VISIBILIDADE_OCULTO
    )
    data_para_ficar_visivel = models.DateTimeField(blank=True, null=True)

    # Sobre a correção
    correcao_automatica = models.BooleanField(default=False)
    correcao_apos_data_limite = models.BooleanField(default=True)
    descontar_um_ponto_a_cada_n_erradas = models.PositiveSmallIntegerField(default=0)
    penalidade_por_dia_atraso = models.PositiveIntegerField(default=1)

    # Sobre a entrega
    data_limite_entrega = models.DateTimeField(blank=True, null=True)
    aceita_envios_apenas_antes_data_limite = models.BooleanField(default=False)
    numero_maximo_tentativas = models.PositiveSmallIntegerField(default=10)
    permitir_resubmissao = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Avaliações"
        ordering = ["disciplina", "numero"]


class QuestaoAvaliacao(models.Model):
    avaliacao = models.ForeignKey(Avaliacao, on_delete=models.PROTECT)
    questao = models.ForeignKey(Questao, on_delete=models.PROTECT)
    numero = models.PositiveSmallIntegerField(default=1)

    class Meta:
        verbose_name_plural = "Questões da avaliacao"
        ordering = ["numero", ]


class GrupoAvaliacao(models.Model):
    avaliacao = models.ForeignKey(Avaliacao, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)


class IntegranteGrupo(models.Model):
    grupo = models.ForeignKey(GrupoAvaliacao, on_delete=models.PROTECT)
    aluno = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)


class Resposta(models.Model):
    # Identificação da resposta
    questao = models.ForeignKey(
        Questao,
        on_delete=models.PROTECT,
        related_name='resposta_questao'
    )
    questao_avaliacao = models.ForeignKey(
        QuestaoAvaliacao,
        on_delete=models.PROTECT,
        related_name='resposta_questaoavaliacao'
    )
    alternativa = models.ForeignKey(
        Alternativa,
        on_delete=models.PROTECT,
        related_name='resposta_alternativa',
        blank=True,
        null=True
    )
    disciplina = models.ForeignKey(Disciplina, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)
    avaliacao = models.ForeignKey(Avaliacao, on_delete=models.PROTECT)

    # Sobre o aluno
    aluno = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    data_submissao = models.DateTimeField(blank=True, null=True)

    # Correção (o professor é quem preenche)
    feedback = RichTextUploadingField(blank=True)
    nota = models.FloatField(default=0)
    corrigido = models.BooleanField(default=False)
    data_correcao = models.DateTimeField(blank=True, null=True)

    # Sobre a resposta
    respondida = models.BooleanField(default=False)
    numero_tentativas = models.PositiveSmallIntegerField(default=0)
    resposta = models.CharField(blank=True, max_length=2)
    conteudo = RichTextUploadingField(blank=True)
    numero_anexos = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        if self.questao.numero_alternativas > 0:
            return 'Questão %s - (%s)' % (self.questao_avaliacao.numero, self.alternativa.codigo)
        else:
            return 'Questão %s' % self.questao_avaliacao.numero

    class Meta:
        verbose_name_plural = "Respostas"
        ordering = ['disciplina', 'questao_avaliacao', 'aluno']


class AnexoResposta(models.Model):
    TIPO_CODIGO_FONTE = 'CF'
    TIPO_DOCUMENTO = 'D'
    TIPO_IMAGEM = 'I'
    TIPO_OUTROS = 'O'
    TIPOS = (
        (TIPO_CODIGO_FONTE, 'Código fonte'),
        (TIPO_DOCUMENTO, 'Documento'),
        (TIPO_IMAGEM, 'Imagem'),
        (TIPO_OUTROS, 'Outros'),
    )

    resposta = models.ForeignKey(Resposta, on_delete=models.PROTECT)
    nome_arquivo = models.CharField(max_length=100)
    tipo = models.CharField(max_length=2, choices=TIPOS, default=TIPO_DOCUMENTO)
    arquivo = models.FileField(upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'anexo_resposta')))
    data_criacao = models.DateField(auto_now_add=True)


class AnexoRelatorio(models.Model):
    TIPO_CODIGO_FONTE = 'CF'
    TIPO_DOCUMENTO = 'D'
    TIPO_IMAGEM = 'I'
    TIPO_OUTROS = 'O'
    TIPOS = (
        (TIPO_CODIGO_FONTE, 'Código fonte'),
        (TIPO_DOCUMENTO, 'Documento'),
        (TIPO_IMAGEM, 'Imagem'),
        (TIPO_OUTROS, 'Outros'),
    )

    relatorio = models.ForeignKey(Avaliacao, on_delete=models.PROTECT)
    nome_arquivo = models.CharField(max_length=100)
    tipo = models.CharField(max_length=2, choices=TIPOS, default=TIPO_DOCUMENTO)
    arquivo = models.FileField(upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'anexo_relatorio')))
    data_criacao = models.DateField(auto_now_add=True)


class NotaAlunoAvaliacao(models.Model):
    data_criacao = models.DateField(auto_now_add=True)
    aluno = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    avaliacao = models.ForeignKey(Avaliacao, on_delete=models.PROTECT)
    nota = models.FloatField(default=0)


class ExperimentoLabRemoto(models.Model):
    avaliacao = models.ForeignKey(
        Avaliacao,
        on_delete=models.PROTECT,
    )

    ensaio = models.ForeignKey(
        Ensaio,
        on_delete=models.PROTECT,
    )

    filmar = models.BooleanField(default=False)
    data_criacao = models.DateField(auto_now_add=True)


@receiver(models.signals.post_delete, sender=AnexoResposta)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    - Deletes file from filesystem
      when corresponding `MediaFile` object is deleted.
    - Quando um aluno remove um anexo, deve-se verificar se ele ainda
      respondeu a questão
    - Deve decrementar o número de anexos na resposta
    """
    if instance.arquivo:
        if os.path.isfile(instance.arquivo.path):
            os.remove(instance.arquivo.path)

    resposta = instance.resposta
    questao = resposta.questao
    if questao.tipo == questao.TIPO_DISSERATIVA:
        if len(resposta.conteudo.split(' ')) == 0:
            resposta.respondida = False

    elif questao.tipo == questao.TIPO_VERDADEIRO_FALSO:
        if resposta.resposta == '':
            resposta.respondida = False

    resposta.numero_anexos = resposta.numero_anexos - 1
    resposta.save()


@receiver(models.signals.pre_save, sender=AnexoResposta)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = AnexoResposta.objects.get(pk=instance.pk).arquivo
    except AnexoResposta.DoesNotExist:
        return False

    new_file = instance.arquivo
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


@receiver(models.signals.post_save, sender=AnexoResposta)
def auto_anexo_criado_resposta_respondida(sender, instance, created, **kwargs):
    """
    Quando um aluno envia um anexo, deve-se marcar que a
    resposta foi respondida automaticamente
    """
    if created is True:
        resposta = instance.resposta
        resposta.respondida = True
        resposta.numero_anexos = resposta.numero_anexos + 1
        resposta.save()


@receiver(models.signals.post_save, sender=Questao)
def auto_incrementa_numero_alternativas(sender, instance, created, **kwargs):
    """
    Quando cria-se uma nova questão,
    incrementa o número de questões na avaliação automaticamente
    """
    if created is True:
        avaliacao = instance.avaliacao
        avaliacao.numero_questoes = avaliacao.numero_questoes + 1
        avaliacao.save()


@receiver(models.signals.post_delete, sender=Questao)
def auto_decrementa_numero_alternativas(sender, instance, **kwargs):
    """
    Quando deleta-se uma questão,
    decrementa o número de questões na avaliação automaticamente
    """
    avaliacao = instance.avaliacao
    avaliacao.numero_questoes = avaliacao.numero_questoes - 1
    avaliacao.save()


@receiver(models.signals.post_save, sender=Alternativa)
def auto_incrementa_numero_alternativas(sender, instance, created, **kwargs):
    """
    Quando cria-se uma nova alternativa,
    incrementa o número de alternativas na questão automaticamente
    """
    if created is True:
        questao = instance.questao
        questao.numero_alternativas = questao.numero_alternativas + 1
        questao.save()


