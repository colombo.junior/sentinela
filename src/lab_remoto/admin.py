from django.contrib import admin
from .models import *


class AdminKitDidatico(admin.ModelAdmin):
    list_display = ('nome', 'numero', 'porta_serial', 'webcam',)


class AdminParametroParaSeremInformados(admin.ModelAdmin):
    list_display = ('nome', 'ensaio', 'sigla', 'valor_minimo', 'valor_maximo',)


class AdminParametrosInformadosPeloUsuario(admin.ModelAdmin):
    list_display = ('usuario', 'ensaio', 'parametro', 'valor_informado',)


class AdminExecucaoEnsaio(admin.ModelAdmin):
    list_display = ('usuario', 'kit', 'ensaio', 'data_inicio', 'data_termino', 'arquivo',)


admin.site.register(KitDidatico, AdminKitDidatico)
admin.site.register(ParametroParaSerInformado, AdminParametroParaSeremInformados)
admin.site.register(ParametroInformado, AdminParametrosInformadosPeloUsuario)
admin.site.register(ExecucaoEnsaio, AdminExecucaoEnsaio)
