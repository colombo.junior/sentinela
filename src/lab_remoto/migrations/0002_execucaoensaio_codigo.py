# Generated by Django 2.2.9 on 2020-08-03 18:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_remoto', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='execucaoensaio',
            name='codigo',
            field=models.TextField(default=''),
        ),
    ]
