from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.deconstruct import deconstructible
from django.dispatch import receiver
from django.conf.global_settings import MEDIA_ROOT

from usuarios.models import Usuario

import os
from uuid import uuid4

# Não relar
@deconstructible
class Caminho(object):
    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)


# class QuarkConfig(models.Model):
#     # Campos que o usuário pode customizar
#     porta_serial = models.CharField(max_length=100)
#     habilitar_encoder_1 = models.BooleanField(default=False)
#     habilitar_encoder_2 = models.BooleanField(default=False)
#
#     def comando_habilitar_encoder_1(self):
#         return 'encoder_1_enable()'
#
#     def comando_habilitar_encoder_2(self):
#         return 'encoder_2_enable()'


class KitDidatico(models.Model):
    nome = models.CharField(
        max_length=100,
        blank=False
    )

    descricao = RichTextUploadingField(blank=True)
    manual_uso = RichTextUploadingField(blank=True)
    manual_manutencao = RichTextUploadingField(blank=True)
    numero = models.PositiveSmallIntegerField(blank=True)
    foto = models.FileField(
        upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'kits_didaticos'))
    )

    data_criacao = models.DateTimeField(auto_now_add=True)
    numero_ensaios_realizados = models.PositiveIntegerField(default=0)
    agendavel = models.BooleanField(default=False)
    em_manutencao = models.BooleanField(default=False)
    numero_ensaios_para_manutencao = models.PositiveIntegerField(default=100)

    webcam = models.CharField(max_length=100, blank=True)
    comando_gravar_video = models.TextField(
        blank=True,
        null=True,
    )

    porta_serial = models.CharField(max_length=100, blank=True)
    ocupado = models.BooleanField(default=False)

    # Número de canais de entrada e saída
    numero_entradas = models.PositiveSmallIntegerField(default=1)
    numero_saidas = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return '%s (#%s)' % (self.nome, self.numero)


class ManutencaoRealizada(models.Model):
    kit = models.ForeignKey(
        KitDidatico,
        on_delete=models.PROTECT,
    )

    laudo = RichTextUploadingField()
    data = models.DateTimeField(blank=True, null=True)


class ParametroDoKit(models.Model):
    kit = models.ForeignKey(
        KitDidatico,
        on_delete=models.PROTECT,
    )

    nome = models.CharField(max_length=1000)
    sigla = models.CharField(max_length=100)
    valor = models.FloatField(default=0.0)
    unidade = models.CharField(max_length=100)


class Ensaio(models.Model):
    kit = models.ForeignKey(
        KitDidatico,
        on_delete=models.PROTECT,
        related_name='ensaio_kit',
        blank=True,
        null=True
    )

    professor = models.ForeignKey(
        Usuario,
        on_delete=models.PROTECT,
    )

    nome = models.CharField(max_length=1000)

    # Esse é o código que executará o experimento com o hardware
    codigo_executavel = models.TextField(blank=True)

    # Esse é o código que gerará o gráfico (usando a biblioteca Bokeh)
    codigo_grafico = models.TextField(blank=True)

    # O enssaio é agendável e/ou executável?
    agendavel = models.BooleanField(default=True)
    executavel = models.BooleanField(default=True)

    def __str__(self):
        return '%s (%s)' % (self.professor, self.nome)


class ParametroParaSerInformado(models.Model):
    nome = models.CharField(max_length=1000)
    ensaio = models.ForeignKey(
        Ensaio,
        on_delete=models.PROTECT,
    )

    # Substituir a
    sigla = models.CharField(max_length=100)

    # por no mínimo
    valor_minimo = models.FloatField(default=0.0)

    # e no máximo
    valor_maximo = models.FloatField(default=0.0)

    # Alguns parâmetros (como tempo de execução e período de amostragem)
    # não podem ser removidos
    pode_ser_removido = models.BooleanField(default=True)


class ExecucaoEnsaio(models.Model):
    ESTADO_AGUARDANDO_PARAMETROS = 'a'
    ESTADO_PARAMETROS_PREENCHIDOS = 'b'
    ESTADO_NA_FILA = 'f'
    ESTADO_EXECUTANDO = 'e'
    ESTADO_RENDERIZANDO_VIDEO = 'r'
    ESTADO_FINALIZADO = 'p'
    ESTADO_OPCOES = (
        (ESTADO_AGUARDANDO_PARAMETROS, 'Aguardando parâmetros'),
        (ESTADO_PARAMETROS_PREENCHIDOS, 'Pronto para executar'),
        (ESTADO_NA_FILA, 'Na fila'),
        (ESTADO_EXECUTANDO, 'Executando'),
        (ESTADO_RENDERIZANDO_VIDEO, 'Renderizando vídeo'),
        (ESTADO_FINALIZADO, 'Finalizado'),
    )
    estado = models.CharField(
        max_length=1,
        default=ESTADO_AGUARDANDO_PARAMETROS,
        choices=ESTADO_OPCOES,
    )

    RESULTADO_NAO_EXECUTADO = 'n'
    RESULTADO_SUCESSO = 's'
    RESULTADO_FALHA = 'f'
    RESULTADO_OPCOES = (
        (RESULTADO_NAO_EXECUTADO, 'Não executado'),
        (RESULTADO_FALHA, 'Falha'),
        (RESULTADO_SUCESSO, 'Sucesso'),
    )
    resultado = models.CharField(
        max_length=1,
        default=RESULTADO_NAO_EXECUTADO,
        choices=RESULTADO_OPCOES,
    )

    usuario = models.ForeignKey(
        Usuario,
        on_delete=models.PROTECT,
    )

    kit = models.ForeignKey(
        KitDidatico,
        on_delete=models.PROTECT,
    )

    ensaio = models.ForeignKey(
        Ensaio,
        on_delete=models.PROTECT,
    )

    # Quando o usuário clicou em executar
    data_inicio = models.DateTimeField(blank=True, null=True)
    data_termino = models.DateTimeField(blank=True, null=True)

    # Arquivo MATLAB com os dados do ensaio
    arquivo = models.FileField(
        upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'dados_ensaios'))
    )

    # Video do experimento
    filmar = models.BooleanField(default=False)
    arquivo_video = models.FileField(
        upload_to=Caminho(os.path.join(MEDIA_ROOT, 'upload', 'dados_ensaios'))
    )

    # Código do experimento (o código usado no experimento)
    codigo = models.TextField(default='')

    # Marcações que os alunos podem fazer
    TAG_NENHUM = 'n'
    TAG_BOM = 'b'
    TAG_RUIM = 'r'
    TAG_DUVIDA = 'd'
    TAG_FAVORITO = 'f'
    TAG_OPCOES = (
        (TAG_NENHUM, 'Nada de especial'),
        (TAG_BOM, 'Resultado bom'),
        (TAG_RUIM, 'Resultado ruim'),
        (TAG_DUVIDA, 'Fiquei com dúvida'),
        (TAG_FAVORITO, 'Favorito'),
    )
    tag = models.CharField(
        default=TAG_NENHUM,
        choices=TAG_OPCOES,
        max_length=1,
    )

    comentario = models.CharField(
        max_length=1000,
        blank=True,
    )

    ip_quem_criou = models.GenericIPAddressField(
        default='0.0.0.0',
    )

    ip_quem_executou = models.GenericIPAddressField(
        default='0.0.0.0',
    )

class ParametroInformado(models.Model):
    usuario = models.ForeignKey(
        Usuario,
        on_delete=models.PROTECT,
    )

    ensaio = models.ForeignKey(
        Ensaio,
        on_delete=models.PROTECT,
    )

    execucao = models.ForeignKey(
        ExecucaoEnsaio,
        on_delete=models.CASCADE,
    )

    parametro = models.ForeignKey(
        ParametroParaSerInformado,
        on_delete=models.PROTECT,
    )

    valor_informado = models.FloatField(default=0.0)

    data = models.DateTimeField(auto_now_add=True)


@receiver(models.signals.post_save, sender=ExecucaoEnsaio)
def auto_incrementa_numero_execucoes(sender, instance, created, **kwargs):
    """
    Quando cria-se uma nova execucação, é necessário incrementar
    o número de ensaios realizados pelo kit.
    """
    if created is True:
        kit = instance.kit
        kit.numero_ensaios_realizados = kit.numero_ensaios_realizados + 1
        kit.save()






