import time
import shlex
import subprocess

webcam = '/dev/video0'
cmd = "ffmpeg -f video4linux2 -input_format h264 -video_size 640x480 -framerate 30 -i /dev/video0 -t 3 -vcodec copy -an arquivo.h264"

processo_gravacao = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE)

eventos = list()
comecou_a_gravar = False
timeout_segundos = 1
ti = time.time()
while comecou_a_gravar is False and timeout_segundos > (time.time() - ti):
    li_isso = processo_gravacao.stdout.readline().decode("utf-8")
    eventos.append(str(li_isso))
    eventos.append('no loop')
    comecou_a_gravar = webcam in str(li_isso)

eventos.append('Eu li o bang e encerrei a gravação!')

processo_gravacao.kill()

time.sleep(3)
print(eventos)


