#!/bin/bash

source venv/bin/activate
cd ./src

# Inicializa o Celery
./start_worker_default.sh &
./start_worker_video.sh &

# Espera o Celery inicializar por completo
sleep 15s

# Inicializa o servidor django
./start_django.sh
