# Django
django==2.2.9
django-crispy-forms
django-ckeditor==5.7.1
Celery[redis]

# PostgreSQL
psycopg2-binary

# Gráficos e imagens
bokeh
pillow

# Bibliotecas matemáticas
numpy
scipy
matplotlib
pyserial


